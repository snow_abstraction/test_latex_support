


```math
\begin{bmatrix}
3 x_0 & - \frac{12316}{324} x_1     &     + 1 x_2 \\
-7x_0  &        -\frac{3}{1} x_1  \\
1 x_0   & &                   -\frac{82340}{23} x_2 
\end{bmatrix} 
\begin{matrix}
= \\
\le \\
\ge
\end{matrix} 
\begin{bmatrix}
\frac{234}{88888888234123412} \\
-100 \\
2
\end{bmatrix}
```


